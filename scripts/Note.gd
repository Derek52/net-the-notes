extends RigidBody2D

export (int) var min_speed
export (int) var max_speed

enum Notes {BLUE_NOTE, RED_NOTE, GREEN_NOTE, PURPLE_NOTE}

var noteType

func _ready():
	noteType = randi() % Notes.size()
	$purpleNoteHitBox.disabled = true
	$redNoteHitBox.disabled = true
	$greenNoteHitBox1.disabled = true
	$greenNoteHitBox2.disabled = true
	$blueNoteHitBox1.disabled = true
	$blueNoteHitBox2.disabled = true
	$blueNoteHitBox3.disabled = true
	$blueNoteHitBox4.disabled = true
	match noteType:
		Notes.BLUE_NOTE:
			$Sprite.texture = load("res://assets/art/blueNote.png")
			$blueNoteHitBox1.disabled = false
			$blueNoteHitBox2.disabled = false
			$blueNoteHitBox3.disabled = false
			$blueNoteHitBox4.disabled = false
		Notes.RED_NOTE:
			$Sprite.texture = load("res://assets/art/redNote.png")
			$redNoteHitBox.disabled = false
		Notes.GREEN_NOTE:
			$Sprite.texture = load("res://assets/art/greenNote.png")
			$greenNoteHitBox1.disabled = false
			$greenNoteHitBox2.disabled = false
		Notes.PURPLE_NOTE:
			$Sprite.texture = load("res://assets/art/purpleNote.png")
			$purpleNoteHitBox.disabled = false

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
