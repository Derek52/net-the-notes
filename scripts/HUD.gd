extends CanvasLayer

signal startGame
signal changeActiveNote
	
func _ready():
	$NewHighScoreLabel.hide()
	
func _process(delta):
	var timeLeft = $ActiveNoteTimer.time_left
	if timeLeft > 2.01:
		$BottomBar/staticTimerLabel.hide()
		$BottomBar/timerLabel.hide()
	else:
		$BottomBar/staticTimerLabel.show()
		$BottomBar/timerLabel.show()
		$BottomBar/timerLabel.text = "%.2f" % $ActiveNoteTimer.time_left

func updateScore(score):
	$ScoreLabel.text = str(score)

func changeNoteToCatch(activeNoteName):
	$BottomBar/NoteToCatchSprite.texture = load("res://assets/art/" + activeNoteName)

func showMessage(text):
	$HudLabel.text = text
	$HudLabel.show()
	$MessageTimer.start()

func showGameOver():
	showMessage("Game Over")
	yield($MessageTimer, "timeout")
	$HudLabel.text = "Collect The Notes!"
	$StartButton.show()

func _on_StartButton_pressed():
	$StartButton.hide()
	$NewHighScoreLabel.hide()
	$HighScoreLabel.hide()
	$HudLabel.hide()
	emit_signal("startGame")

func _on_MessageTimer_timeout():
	$HudLabel.hide()

func _on_ActiveNoteTimer_timeout():
	emit_signal("changeActiveNote")
	$ActiveNoteTimer.set_wait_time(randf() + (randi() % 6) + 3) #sets the timer to anywhere from 3 to 8.9999
	
func startActiveNoteTimer():
	$ActiveNoteTimer.start()
	
func stopActiveNoteTimer():
	$ActiveNoteTimer.stop()

func showHighScore(highScore, newHighScore):
	$HighScoreLabel.text = "High Score: " + str(highScore)
	$HighScoreLabel.show()
	if newHighScore:
		$NewHighScoreLabel.show()
