extends Area2D

signal dead #player has hit a dangerous note, and game is over
signal caughtNote

export (int) var speed
var screensize

var activeNoteType

func _ready():
	screensize = get_viewport_rect().size
	hide()

func start(pos):
	position = pos
	show()
	$CollisionShape2D.disabled = false

func _process(delta):
	var velocity = Vector2()
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
		
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		
	position += velocity * delta
	position.x = clamp(position.x, 0, screensize.x)
	position.y = clamp(position.y, 0, screensize.y - 70)



func _on_Player_body_entered(body):
	if body.noteType == activeNoteType:
		emit_signal("caughtNote")
		body.queue_free()
	else:
		emit_signal("dead")
		$CollisionShape2D.disabled = true
		$DeadSound.play()
	
