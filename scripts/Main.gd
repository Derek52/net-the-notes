extends Node

export (PackedScene) var Note
var score = 0
var highScore

signal changeActiveNoteType

enum Notes {BLUE_NOTE, RED_NOTE, GREEN_NOTE, PURPLE_NOTE}
var scoringNoteType

func _ready():
	randomize()
	highScore = getHighScore()
	$HUD.showHighScore(highScore, false)

func _on_Player_caughtNote():
	score += 1
	$AudioStreamPlayer.play()
	$HUD.updateScore(score)

func _on_SpawnNoteTimer_timeout():
	$NotePath/NoteSpawnPath.offset = randi()
	var note = Note.instance()
	$Notes.add_child(note)
	var direction = $NotePath/NoteSpawnPath.rotation + PI / 2
	note.position = $NotePath/NoteSpawnPath.position
	note.scale = Vector2(.5, .5)
	direction += rand_range(-PI / 4, PI / 4)
	#note.rotation = direction
	note.set_linear_velocity(Vector2(rand_range(note.min_speed, note.max_speed), 0).rotated(direction))

func updateActiveNote():
	var activeNoteName
	match scoringNoteType:
		Notes.BLUE_NOTE:
			activeNoteName = "blueNote.png"
			$ColorRect.color = Color("#4DA6FF")
			$AudioStreamPlayer.stream = load("res://assets/sfx/noteSound_1.wav")
		Notes.RED_NOTE:
			activeNoteName = "redNote.png"
			$ColorRect.color = Color("#FF6666")
			$AudioStreamPlayer.stream = load("res://assets/sfx/noteSound_2.wav")
		Notes.PURPLE_NOTE:
			activeNoteName = "purpleNote.png"
			$ColorRect.color = Color("#FF33DD")
			$AudioStreamPlayer.stream = load("res://assets/sfx/noteSound_3.wav")
		Notes.GREEN_NOTE:
			activeNoteName = "greenNote.png"
			$ColorRect.color = Color("#33FF55")
			$AudioStreamPlayer.stream = load("res://assets/sfx/noteSound_4.wav")
	$HUD.changeNoteToCatch(activeNoteName)

func newGame():
	deleteAllNotes()
	score = 0
	$HUD.updateScore(score)
	$Player.start($StartPosition.position)
	$Player.show()
	$SpawnNoteTimer.start()
	scoringNoteType = randi() % Notes.size()
	$Player.activeNoteType = scoringNoteType
	print(scoringNoteType)
	$HUD.startActiveNoteTimer()
	updateActiveNote()

func deleteAllNotes():
	for child in $Notes.get_children():
		child.queue_free()

#this method is connected to the dead signal from the player scene
func _on_game_over():
	$SpawnNoteTimer.stop()
	$Player.hide()
	$HUD.stopActiveNoteTimer()
	$HUD.showGameOver()
	var newHighScore = false
	if score > highScore:
		highScore = score
		newHighScore = true
		saveHighScore()
	$HUD.showHighScore(highScore, newHighScore)

func _on_HUD_startGame():
	newGame()

func _on_HUD_changeActiveNote():
	var newNoteType = randi() % Notes.size()
	while scoringNoteType == newNoteType:
		newNoteType = randi() % Notes.size()
	scoringNoteType = newNoteType
	$Player.activeNoteType = scoringNoteType
	emit_signal("changeActiveNoteType", scoringNoteType)
	updateActiveNote()
	
func saveHighScore():
	var highScoreFile = File.new()
	highScoreFile.open("user://high_score.save", File.WRITE)
	highScoreFile.store_line(str(highScore))
	highScoreFile.close()
	
func getHighScore():
	var highScoreFile = File.new()
	if not highScoreFile.file_exists("user://high_score.save"):
		return 0#save file doesn't exist yet
	
	highScoreFile.open("user://high_score.save", File.READ)
	var highScore = highScoreFile.get_line() #this code only works, because I'll only ever save one thing to this file. 
	#Do not copy this method. Read about data persistence in the godot docs, here http://docs.godotengine.org/en/3.0/tutorials/io/saving_games.html?highlight=save
	highScoreFile.close()
	return int(highScore)
