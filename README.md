# Net The Notes

This is a fun little game made with the Godot game engine. I made it for a music themed game jam, you can check out all of the other entries [here](https://itch.io/jam/musicgamejam2018).

You can view my game directly on Itch, [here](https://derek52.itch.io/net-the-notes).

I had an idea for a more complicated game, but ended up only having about a day to work on a game for this game jam, so I came up with the idea for this game, and published it, in about 24 hours. Which is something everyone should try. It was a lot of fun.

The source code for my game, just like the Godot engine itself, is under the MIT license.
